package com.github.mcheung63.netbeans.minimap;

import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.openide.util.NbPreferences;

public class SideBarFactory implements org.netbeans.spi.editor.SideBarFactory {

	public static ArrayList<MinimapPanel> minimapPanels = new ArrayList<MinimapPanel>();

	@Override
	public JComponent createSideBar(JTextComponent jtc) {
		MinimapPanel minimapPanel = new MinimapPanel(jtc);
		minimapPanel.setVisible(MenuAction.visible);
		minimapPanels.add(minimapPanel);
		int width = NbPreferences.forModule(MinimapPanel.class).getInt("width", 100);
		minimapPanel.setPreferredSize(new Dimension(width, 500));
		return minimapPanel;
	}
}
