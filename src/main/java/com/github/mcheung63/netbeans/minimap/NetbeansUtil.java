package com.github.mcheung63.netbeans.minimap;

import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

/**
 *
 * @author peter, mcheung63@hotmail.com
 */
public class NetbeansUtil {

	public static Project getProject() {
		Lookup context = Utilities.actionsGlobalContext();
		Project project = context.lookup(Project.class);
		if (project == null) {
			Node node = context.lookup(Node.class);
			if (node != null) {
				DataObject dataObject = node.getLookup().lookup(DataObject.class);
				if (dataObject == null) {
					return null;
				}
				FileObject fileObject = dataObject.getPrimaryFile();
				project = FileOwnerQuery.getOwner(fileObject);
			}
		}

		return project;
	}
}
