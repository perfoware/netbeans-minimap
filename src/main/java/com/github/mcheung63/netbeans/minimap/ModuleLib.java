package com.github.mcheung63.netbeans.minimap;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

/**
 *
 * @author peter
 */
public class ModuleLib {

	public static boolean isDebug = false;
	static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:s.S");

	public static void log(String str) {
		if (isDebug) {
			InputOutput io = IOProvider.getDefault().getIO("Netbeans minimap", false);
			io.getOut().println(sdf.format(new Date()) + " - " + str);
		}
	}

	public static void log(Object obj) {
		log(obj.toString());
	}
}
