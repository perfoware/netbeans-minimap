# netbeans-minimap

Display the code minimap, programmer can overlook their code.

1. Grey color
2. Ignore middle white space
3. Show current line

# Setting

Right click the panel, click "setting"

<img src="https://github.com/mcheung63/netbeans-minimap/blob/master/screen/setting.png?raw=true" width="396">

<img src="https://github.com/mcheung63/netbeans-minimap/blob/master/screen/setting%20dialog.png?raw=true" width="395">

# Grey color

<img src="https://github.com/mcheung63/netbeans-minimap/blob/master/screen/grey-color.png?raw=true" width="288">

# Ignore middle white space

<img src="https://github.com/mcheung63/netbeans-minimap/blob/master/screen/ignore-white-space.png?raw=true" width="396">

